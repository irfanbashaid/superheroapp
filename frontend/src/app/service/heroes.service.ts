import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})

export class HeroesService implements OnInit{
  
  public allSuperHeroes;
 
  constructor(private http:HttpClient) {
    
  }


  public async getAllSuperHeroes():Promise<any>{
    return new Promise((resolve,reject)=>{
      let meta = this;
      if(meta.allSuperHeroes !== undefined && meta.allSuperHeroes.length >0){
        return resolve(meta.allSuperHeroes);
      }
      meta.http.get("/allsuperheroes").subscribe(
      _allSuperHeroes => {
        _allSuperHeroes[407]["images"]["sm"] = "./assets/images/ghul.png";
        _allSuperHeroes[407]["images"]["lg"] = "./assets/images/ghul.png";
        meta.allSuperHeroes = _allSuperHeroes;
        return resolve(_allSuperHeroes);
      },error =>{
        return reject(error);
      });
    });
  }

  ngOnInit(){
    
  }
}
