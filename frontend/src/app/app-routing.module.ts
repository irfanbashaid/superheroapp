import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeroesshowComponent } from "./heroesshow/heroesshow.component";
import { SearchComponent } from './search/search.component';

const routes: Routes = [
  {
    path:'',
    component: HeroesshowComponent,
    pathMatch:'full'
  },
  {
    path:'search',
    component:SearchComponent,
    pathMatch:'full'
  },
  {
    path:'*',
    component:HeroesshowComponent,
    pathMatch:'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
