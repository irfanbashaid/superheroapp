import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeroesshowComponent } from './heroesshow.component';

describe('HeroesshowComponent', () => {
  let component: HeroesshowComponent;
  let fixture: ComponentFixture<HeroesshowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeroesshowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroesshowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
