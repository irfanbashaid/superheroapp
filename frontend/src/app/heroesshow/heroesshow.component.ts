import { Component, OnInit } from '@angular/core';
import { HeroesService } from '../service/heroes.service';

@Component({
  selector: 'app-heroesshow',
  templateUrl: './heroesshow.component.html',
  styleUrls: ['./heroesshow.component.css']
})
export class HeroesshowComponent implements OnInit {

  heroes;
  heroSelected;
  modalShow:boolean = false;
  isImgLoaded:boolean[] = new Array(563);
 
  constructor(private heroserve:HeroesService) {
    let meta = this;
    meta.loadAllHeroes();
    for(let i=0;i<meta.isImgLoaded.length;i++){
      meta.isImgLoaded[i] = false;
    }
    // console.log(meta.isImgLoaded)
  }
  loadAllHeroes(){
    let meta = this;
    meta.heroserve.getAllSuperHeroes().then(
      _allSuperHeroes => {
        meta.heroes = _allSuperHeroes;
        // console.log(_allSuperHeroes.length)
        meta.heroSelected = _allSuperHeroes[0];
        meta.modalShow = true;
      }
    );
  }
  showHero(hero){
    let meta = this;
    meta.heroSelected = hero;
    (document.getElementById("appearanceId") as HTMLLinkElement).click();
  }



  ngOnInit() {
  }

}
