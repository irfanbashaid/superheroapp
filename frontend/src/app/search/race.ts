export class Race{
    public static race:String[] =["Human","Icthyo Sapien","Ungaran","Human / Radiation","Cosmic Entity","Cyborg","Xenomorph XX121","Android","Vampire","Mutant","God / Eternal","Symbiote","Atlantean","Alien","Neyaphem","New God","Alpha","Bizarro","Inhuman","Metahuman","Demon","Human / Clone","Human-Kree","Dathomirian Zabrak","Amazon","Human / Cosmic","Human / Altered","Kryptonian","Kakarantharaian","Black Racer","Zen-Whoberian","Strontian","Kaiju","Saiyan","Gorilla","Rodian","Flora Colossus","Human-Vuldarian","Asgardian","Demi-God","Eternal","Gungan","Bolovaxian","Animal","Czarnian","Martian","Spartoi","Planet","Luphomoid","Parademon","Yautja","Maiar","Clone","Talokite","Korugaran","Zombie","Human-Vulcan","Human-Spartoi","Tamaranean","Frost Giant","Mutant / Clone","Yoda's species","Others"];
} 

export class Gender{
    public static gender:String[] = ["Male", "Female", "Others"];
}

export class Publisher{
    public static publisher:String[] = ["Marvel Comics","Dark Horse Comics","DC Comics","NBC - Heroes","Wildstorm","Archangel","Tempest","Giant-Man","Toxin","Angel","Goliath","Oracle","Spoiler","Nightwing","Icon Comics","SyFy","George Lucas","Meltdown","Gemini V","South Park","Binary","ABC Studios","Universal Studios","Star Trek","Evil Deadpool","IDW Publishing","Deadpool","Black Racer","Speed Demon","Impulse","Shueisha","Sony Pictures","J. K. Rowling","Batgirl III","Flash IV","Titan Books","Phoenix","Power Woman","Rebellion","Iron Lad","Power Man","Image Comics","Microsoft","Boom-Boom","Batgirl V","She-Thing","Batman II","Batgirl","Jean Grey","Robin II","Robin III","Red Hood","Red Robin","J. R. R. Tolkien","Spider-Carnage","Venom III","Ms Marvel II","Aztar","Superman Prime One-Million","Angel Salvadore","Rune King Thor","Anti-Venom","Scorpion","Vindicator II","Anti-Vision","Thunderbird II","Ant-Man","Others"];
}
