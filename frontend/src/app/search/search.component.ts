import { Component, OnInit } from '@angular/core';
import { HeroesService } from '../service/heroes.service';
import { Gender, Race, Publisher  } from "./race";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  constructor(private heroserve:HeroesService) {
    let meta = this;
    meta.loadOrigin();
   }

  filteredHeroes = [];
  modalShow:boolean = false;
  isImgLoaded:boolean[] = new Array(563);
  heroSelected;
  searchValue = '';
  selectedCategory = "name"
  
  public race = Race.race; //null
  public gender = Gender.gender; //-
  public publisher = Publisher.publisher; // "" "null"

  public selectedGender = "select Gender";
  public selectedRace = "Select Race";
  public selectedPublisher= "Select publisher";
  
  ngOnInit() {
  }

  genderChange(_gender){
    let meta = this;
    meta.selectedGender = _gender;
    const matches = meta.heroserve.allSuperHeroes.filter(s =>{
      if(_gender == "Others"){
        return s.appearance.gender != "Male" && s.appearance.gender != "Female";
      }
      return s.appearance.gender == _gender;
    });
    meta.filteredHeroes = matches;
  }

  raceChange(_race){
    let meta = this;
    meta.selectedRace = _race;
    if(_race == "Others"){
      _race = "null";
    }
    const matches = meta.heroserve.allSuperHeroes.filter(s => s.appearance.race == _race);
    meta.filteredHeroes = matches;
  }

  publisherChange(_publisher){
    let meta = this;
    meta.selectedPublisher = _publisher;
    // "" "null"
    const matches = meta.heroserve.allSuperHeroes.filter(s => {
      if(_publisher == "Others"){
        return s.biography.publisher == "" || s.biography.publisher == "null";
      }
      return s.biography.publisher == _publisher;
    });
    meta.filteredHeroes = matches;
  }

  initializeSelection(){
    let meta = this;
    meta.selectedGender = "select Gender";
    meta.selectedRace = "Select Race";
    meta.selectedPublisher= "Select publisher";
    meta.searchValue = "";
    meta.filteredHeroes = [];
  }
  onSearchChange(){  
    let meta = this;
    meta.searchValue = meta.searchValue.trim(); 

    if(meta.searchValue == ""){
      meta.filteredHeroes = [];
      return;
    }
    meta.searchValue = meta.searchValue.toLowerCase();
    for(let i=0;i<meta.isImgLoaded.length;i++){
      meta.isImgLoaded[i] = false;
    }
    const matches = meta.heroserve.allSuperHeroes.filter(s => s.name.toLowerCase().includes(meta.searchValue));
    meta.filteredHeroes = matches;
    
  }

  loadOrigin(){
    let meta = this;
    meta.heroserve.getAllSuperHeroes().then(
      _allSuperHeroes => {
        meta.heroSelected = _allSuperHeroes[0];
        meta.modalShow = true;
      }
    );
  }

  categoryChange(_selectedCategory){
    let meta = this;
    meta.initializeSelection();
    meta.selectedCategory = _selectedCategory;
  }

  showHero(hero){
    let meta = this;
    meta.heroSelected = hero;
    (document.getElementById("appearanceId") as HTMLLinkElement).click();
  }

}
